# README #

Integrate GitLab OAuth2 to allow project members to preview static web sites.

### How do I get set up? ###

* maven goals: spring-boot:run
* maven goals: package spring-boot:repackage
* windows command line: java -Dspring.profiles.active=prod -jar C:\git\gitlabdemosite\target\GitLabDemoSite-0.0.1-SNAPSHOT.jar --spring.config.location=x:/

### Contribution guidelines ###

* Anything you wish