/*
 * ===========================================================================
 * IBM Confidential
 * AIS Source Materials
 * 
 * 
 * (C) Copyright IBM Corp. 2016.
 *
 * ===========================================================================
 */
package org.bearzone.gitlabdemoweb;


/**
 * <p> OAuth2 Token Container</p>
 *
 * @author  Kenneth
 * @version 1.0, 2016年2月15日
 * @see	    
 * @since 
 */
public class Token {
	private boolean present;
	
	private String token;
	
	public Token(String token) {
		if (token != null) {
			present = true;
			this.token = token;
		}
	}

	/**
	 * 取得 present
	 * @return 傳回 present。
	 */
	public boolean isPresent() {
		return present;
	}

	/**
	 * 設定 present
	 * @param present 要設定的 present。
	 */
	public void setPresent(boolean present) {
		this.present = present;
	}

	public String get() {
		return this.token;
	}
}



 