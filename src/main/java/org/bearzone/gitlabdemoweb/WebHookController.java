/*
 * ===========================================================================
 * IBM Confidential
 * AIS Source Materials
 * 
 * 
 * (C) Copyright IBM Corp. 2016.
 *
 * ===========================================================================
 */
package org.bearzone.gitlabdemoweb;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * <p>同步Git上的Repository</p>
 *
 * @author  Kenneth
 * @version 1.0, 2016年4月25日
 * @see	    
 * @since 
 */
@Controller
public class WebHookController {
	protected transient final Logger logger = LoggerFactory.getLogger(WebHookController.class);
	
	@Value("${git.localpath2}")
	private String localRootPath2;

	@Value("${git.baseGitUrl}")
	private String baseGitUrl;

	@Value("${git.username}")
	private String username;

	@Value("${git.password}")
	private String password;

	
	/**
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/webhook/{groupName}/{projectName}", method = RequestMethod.POST)
	public ResponseEntity<String> doCloneOrUpdate(HttpServletRequest request, @PathVariable String groupName,@PathVariable String projectName, @RequestBody String data) {
		logger.info("here: " + request.getRequestURI());
		try {
			this.cloneOrUpdate(baseGitUrl + groupName + "/" + projectName + ".git", groupName + "_" + projectName);
		} catch (Exception e) {
			logger.warn("ERR",e);
		}
		
		return new ResponseEntity<String>(HttpStatus.OK);
	}
		
	private void cloneOrUpdate(String url, String projectName) throws InvalidRemoteException, TransportException, IOException, GitAPIException {
		File localPath = new File(localRootPath2 + "/" + projectName);
		if (localPath.exists()) {
			logger.info("do pull");
			this.pull(localPath, url,projectName);
			
		} else {
			logger.info("do clone");
			this.clone(localPath, url,projectName);
		}
	}
	
	private void clone(File localPath, String url, String projectName) throws IOException, InvalidRemoteException, TransportException, GitAPIException {
	    Git.cloneRepository()
	        .setURI(url)
	        .setDirectory(localPath)
	        .setCredentialsProvider(new UsernamePasswordCredentialsProvider(username, password))
	        .call();
	}
	
	private void pull(File localPath, String url, String projectName) throws IOException, InvalidRemoteException, TransportException, GitAPIException {
	    Git.open(localPath)
	        .pull()
	        .setCredentialsProvider(new UsernamePasswordCredentialsProvider(username, password))
	    	.call();
	}
	
}



 