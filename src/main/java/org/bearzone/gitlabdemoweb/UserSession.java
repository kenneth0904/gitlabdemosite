/*
 * ===========================================================================
 * IBM Confidential
 * AIS Source Materials
 * 
 * 
 * (C) Copyright IBM Corp. 2016.
 *
 * ===========================================================================
 */
package org.bearzone.gitlabdemoweb;

import java.util.Set;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

/**
 * <p> Web Session Object</p>
 *
 * @author  Kenneth
 * @version 1.0, 2016年2月15日
 * @see	    
 * @since 
 */
@Component
@Scope(value="session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserSession {
	private String accessToken;
	
	private Set<String> accessibleProjects;
	
	private String originalURI;
	
	public void saveOAuthToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public Token getToken() {
		if (accessToken != null) {
			return new Token(accessToken);
		} else {
			return new Token(null);
		}
	}
	
	public boolean isAccessible(String projectName) {
		if (accessibleProjects != null && accessibleProjects.contains(projectName)) {
			return true;
		} else {
			return false;
		}
	}

	public void setAccessibleProjects(Set<String> accessibleProjects2) {
		this.accessibleProjects = accessibleProjects2;
	}

	public void setOriginalURI(String requestURI) {
		this.originalURI = requestURI;
	}
	
	public String getOriginalURI() {
		return this.originalURI;
	}	
}



 