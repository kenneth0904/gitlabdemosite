package org.bearzone.gitlabdemoweb;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.PreDestroy;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.utils.DateUtils;
import org.apache.oltu.oauth2.client.OAuthClient;
import org.apache.oltu.oauth2.client.URLConnectionClient;
import org.apache.oltu.oauth2.client.request.OAuthBearerClientRequest;
import org.apache.oltu.oauth2.client.request.OAuthClientRequest;
import org.apache.oltu.oauth2.client.response.OAuthJSONAccessTokenResponse;
import org.apache.oltu.oauth2.client.response.OAuthResourceResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.types.GrantType;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

/**
 * Web Controller
 */
@Controller
public class IndexController {
	protected transient final Logger logger = LoggerFactory.getLogger(IndexController.class);

	@Value("${oauth2.server.url}")
	private String gitlabServerUrl;

	@Value("${oauth2.server.url.authorize.path}")
	private String authorizePath;

	@Value("${oauth2.server.url.token.path}")
	private String tokenPath;

	@Value("${oauth2.client.id}")
	private String clientId;

	@Value("${oauth2.client.secret}")
	private String clientSecret;

	@Value("${oauth2.client.callback.url}")
	private String callbackUrl;

	@Value("${git.localpath2}")
	private String localRootPath;

	@Autowired
	UserSession session;

	@Autowired
	Jackson2ObjectMapperBuilder jsonMapper;

	OAuthClient oAuthClient = new OAuthClient(new URLConnectionClient());

	@PreDestroy
	public void cleanUp() {
		oAuthClient.shutdown();
	}

	/**
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/projects/{projectName}/**", method = RequestMethod.GET)
	public String home(@PathVariable String projectName, HttpServletRequest request, HttpServletResponse response) {
		logger.info("here: " + request.getRequestURI());
		if (!session.getToken().isPresent()) {
			session.setOriginalURI(request.getRequestURI());
			return "redirect:/signIn";
		}
		else if (!session.isAccessible(projectName)) {
			return "redirect:/notAuthorized?projectName=" + projectName;
		}

		String requestURI = request.getRequestURI();
		String relativePath = requestURI.replaceFirst("^/demoSite/projects/", "");

		Path path = Paths.get(localRootPath).resolve(relativePath);
		try {
			String contentType = Files.probeContentType(path);
			logger.debug("PATH[{}] existed[{}] contentType[{}]", path, path.toFile().exists(), contentType);

			File file = path.toFile();
			if (file.exists()) {
				if (!isModified(request, response, file)) {
					return null;
				}
				addCacheHeader(response, file, contentType);
				InputStream is = new FileInputStream(file);
				IOUtils.copy(is, response.getOutputStream());
				response.flushBuffer();
				return null;
			}
			else {
				logger.warn("PATH[{}] existed[{}] contentType[{}]", path, file.exists(), contentType);
			}
		}
		catch (IOException ex) {
			logger.error("Error writing file to output stream. Path: '" + path + "', requestURI: '" + requestURI + "'");
		}
		response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		return null;
	}

	/**
	 * identify is modified or not
	 * 
	 * @param request
	 * @param file
	 * @return
	 */
	private boolean isModified(HttpServletRequest request, HttpServletResponse response, File file) {
		try {
			String dateString = request.getHeader("If-Modified-Since");
			if (!StringUtils.isEmpty(dateString)) {
				Date httpDate = DateUtils.parseDate(dateString);
				Calendar fileDate = Calendar.getInstance();
				fileDate.setTimeInMillis(file.lastModified());
				fileDate.set(Calendar.MILLISECOND, 0);

				logger.info("http time[{}]  file time[{}] url[{}]", httpDate.getTime(), fileDate.getTimeInMillis(), request.getRequestURI());

				if (fileDate.getTimeInMillis() == httpDate.getTime()) {
					response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
					return false;
				}
			}
		}
		catch (Exception e) {
			logger.warn("ERR", e);
		}
		return true;
	}

	int CACHE_DURATION_IN_SECOND = 60 * 5; // 5 minutes

	long CACHE_DURATION_IN_MS = CACHE_DURATION_IN_SECOND * 1000;

	private void addCacheHeader(HttpServletResponse res, File file, String contentType) {
		if (contentType != null) {
			res.setContentType(contentType);
		}

		res.addHeader("Cache-Control", "max-age=" + CACHE_DURATION_IN_SECOND);
		res.addHeader("Cache-Control", "must-revalidate");// optional
		res.setDateHeader("Last-Modified", file.lastModified());
		res.setDateHeader("Expires", System.currentTimeMillis() + CACHE_DURATION_IN_MS);
	}

	/**
	 * Not Authorized Page
	 * 
	 * @param projectName
	 * @return
	 */
	@RequestMapping("/notAuthorized")
	@ResponseBody
	public String notAuthorized(@RequestParam String projectName) {
		return "not authorized to access project: " + projectName;
	}

	/**
	 * Default target page
	 */
	@RequestMapping("/main")
	@ResponseBody
	public String main() {
		if (session.getToken().isPresent()) {
			return "authorization is done, you are good to go with access token: " + session.getToken().get();
		}
		else {
			return "no authority.";
		}
	}

	/**
	 * Do OAuth login
	 * 
	 * @param req
	 * @param response
	 * @return
	 * @throws Throwable
	 */
	@RequestMapping("/signIn")
	public String index(HttpServletRequest req, HttpServletResponse response) throws Throwable {
		if (session.getToken().isPresent()) {
			if (StringUtils.isEmpty(session.getOriginalURI())) {
				return "redirect:/main";
			}
			else {
				String path = "redirect:" + session.getOriginalURI().replaceFirst(req.getContextPath(), "");
				session.setOriginalURI(null);
				return path;
			}
		}
		else {
			logger.info("first login, build oauth request >..");
			OAuthClientRequest request = OAuthClientRequest.authorizationLocation(gitlabServerUrl + authorizePath).setClientId(clientId).setRedirectURI(callbackUrl).setResponseType("code").buildQueryMessage();

			String gitlabAuthUrl = request.getLocationUri();

			logger.info("redirect to : " + gitlabAuthUrl);
			return "redirect:" + gitlabAuthUrl;
		}
	}

	/**
	 * OAuth Callback
	 * 
	 * @param code
	 * @param error
	 * @param errorDescription
	 * @return
	 * @throws Throwable
	 */
	@RequestMapping("/receiveAuth")
	public String callback(HttpServletRequest req, @RequestParam(value = "code", required = false) String code, @RequestParam(value = "error", required = false) String error, @RequestParam(value = "error_description", required = false) String errorDescription) throws Throwable {

		if (StringUtils.hasLength(error)) {
			logger.error("authorization fails with error={} and error description={}", error, errorDescription);
		}
		else {
			logger.info("callback request receives with code={}", code);

			OAuthClientRequest request = OAuthClientRequest.tokenLocation(gitlabServerUrl + tokenPath).setGrantType(GrantType.AUTHORIZATION_CODE).setClientId(clientId).setClientSecret(clientSecret).setRedirectURI(callbackUrl)
					.setGrantType(GrantType.AUTHORIZATION_CODE).setCode(code).buildQueryMessage();

			logger.info("build authorize request with code:{} and client secret", code);

			OAuthJSONAccessTokenResponse response = oAuthClient.accessToken(request);

			String accessToken = response.getAccessToken();
			logger.info("access token got: {}", accessToken);

			session.saveOAuthToken(accessToken);

			Set<String> accessibleProjects = getAccessibleProjects();
			logger.info("accessible projects: {}", accessibleProjects);
			session.setAccessibleProjects(accessibleProjects);

			// TreeNode userInfo = getUserInfo();
			// logger.info("userinfo treeNode: {}", userInfo);
		}

		if (StringUtils.isEmpty(session.getOriginalURI())) {
			return "redirect:/main";
		}
		else {
			String path = "redirect:" + session.getOriginalURI().replaceFirst(req.getContextPath(), "");
			return path;
		}

	}

	/**
	 * Get a list of projects accessible by the authenticated user.
	 */
	private Set<String> getAccessibleProjects() throws OAuthSystemException, OAuthProblemException, JsonProcessingException, IOException {
		OAuthClientRequest bearerClientRequest = new OAuthBearerClientRequest(gitlabServerUrl + "/api/v3/projects").setAccessToken(session.getToken().get()).buildQueryMessage();
		OAuthResourceResponse resourceResponse = oAuthClient.resource(bearerClientRequest, OAuth.HttpMethod.GET, OAuthResourceResponse.class);

		Set<String> accessibleProjects = new HashSet<String>();
		JsonNode treeNode = jsonMapper.build().readTree(resourceResponse.getBody());
		for (JsonNode root : treeNode) {
			String name = root.get("name").textValue();
			String namespace = root.get("namespace").get("name").textValue();
			accessibleProjects.add(String.format("%s_%s", namespace, name));
		}
		return accessibleProjects;
	}

	// /**
	// * Get authenticated user info
	// *
	// * @return
	// * @throws OAuthSystemException
	// * @throws OAuthProblemException
	// * @throws JsonProcessingException
	// * @throws IOException
	// */
	// private TreeNode getUserInfo() throws OAuthSystemException,
	// OAuthProblemException, JsonProcessingException, IOException {
	// OAuthClientRequest bearerClientRequest = new
	// OAuthBearerClientRequest(gitlabServerUrl +
	// "/api/v3/user").setAccessToken(session.getToken().get()).buildQueryMessage();
	// OAuthResourceResponse resourceResponse =
	// oAuthClient.resource(bearerClientRequest, OAuth.HttpMethod.GET,
	// OAuthResourceResponse.class);
	// TreeNode treeNode =
	// jsonMapper.build().readTree(resourceResponse.getBody());
	// return treeNode;
	// }

}